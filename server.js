const http = require("http");
const fs = require("fs");

http.createServer((request, response) => {
    response.setHeader("Content-Type", "text/html; charset=utf-8")
    if(request.method === "GET"){
        switch(request.url){
            case "/": 
                response.end("Hola mundo");
                break;
            case "/contacto":
                response.end("Página de contacto");
                break;
            case "/protegido":
                response.setHeader("Location", "/");
                response.statusCode = 302;
                response.end();
                break;
            case "/usuarios":
                fs.readFile("usuarios_db.json", (error, content) => {
                    let usuarios = JSON.parse(content);
    
                    response.end(`<ul>
                        <li>${usuarios[0].name}</li>
                        <li>${usuarios[1].name}</li>
                        <li>${usuarios[2].name}</li>
                        <li>${usuarios[3].name}</li>
                    </ul>`);
                });
                break;
        }
    }else if(request.method === "POST"){
        switch(request.url){
            case "/usuarios":
                // fs.writeFile("usuarios_db.json", JSON.stringify({name: "Gustavo"}), error => {
                //     console.log("No se pudo escribir sobre el archivo usuarios_db.json");
                // });
                // fs.appendFile("usuarios_db.json", JSON.stringify({name: "Gustavo"}), error => {
                //     console.log("No se pudo escribir sobre el archivo usuarios_db.json");
                // });

                let newUser = "";

                request.on("data", (chunk) => {
                    newUser += chunk.toString();
                });

                request.on("end", () => {
                    fs.readFile("usuarios_db.json", (error, content) => {
                        let usuarios = JSON.parse(content);
                        // [0] -> name=Oscar [1] -> lastname=Islas 
                        // [0] -> name [1] -> Oscar [2] -> lastname [3] -> Islas
                        newUser = newUser.split("&");
                        newUser = newUser[0].split("=")[1];

                        usuarios.push({name: newUser});
                        fs.writeFile("usuarios_db.json", JSON.stringify(usuarios), error => {
                            console.log("No se pudo escribir sobre el archivo usuarios_db.json");
                        });
                        response.end("El usuario ha sido registrado correctamente");
                    });
                });
                break;
        }
    }
}).listen(8000);